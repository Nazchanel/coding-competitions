Collecting Beepers
---

Karel is a robot who lives in a rectangular coordinate system where
each place is designated by a set of integer coordinates ( and ).
Your job is to design a program that will help Karel pick up a
number of beepers that are placed in her world. To do so you must
direct Karel to the position where each beeper is located. Your job is
to write a computer program that 