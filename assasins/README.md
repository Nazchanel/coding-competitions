Assassins
=========
In the cut-throat world of assassins for hire, the rivalry is ruthless and everyone is fighting to get an edge. To eliminate the competition, many assassins even go so far as to assassinate other assassins. The question is: with several assassins trying to do each other in, which ones will remain alive and kicking, and which ones will kick the bucket?

Assassins generally lay careful plans before executing them, including planning multiple attempts to dispose of the same target, with the second attempt being a backup in case the first attempt fails, the third attempt being a secondary backup, and so on. Using their great annihilytical skills, assassins can also very accurately determine the probability that any given assassination attempt will succeed.

Given the list of planned assassination attempts for a group of assassins, what are the probabilities that each assassin is alive after all these attempts? Performing an assassination attempt requires that the assassin is still alive, so if the assassin is indisposed due to already having been assassinated, the attempt is cancelled.

Input
-----

The first line of input contains two integers $n$ and $m$, where $n$ $(1 \leq n \leq 15)$ is the number of assassins, and $m$ $(0 \leq m \leq 1000)$ is the number of planned assassination attempts. The assassins are numbered from $1$ to $n$.

Then follow $m$ lines, each containing two integers $i$, $j$, and a real number $p$, indicating that assassin $i$ plans to attempt to assassinate assassin $j$ $(1 \leq i,  j \leq n,  j \neq i)$, and that this attempt would succeed with probability $p$ $(0 \leq p \leq 1$, at most $6$ digits after the decimal point). The planned attempts are listed in chronological order from first to last, and no two attempts happen simultaneously.

Output
------

Output $n$ lines, with the $i$’th containing the probability that assassin $i$ is alive after all $m$ assassination attempts have taken place. You may assume that none of the $n$ assassins will die of any other cause than being assassinated in one of these $m$ attempts. The probababilities should be accurate to an absolute error of at most $10^{-6}$.
|  Sample Input 1 |Sample Output 1   |
|---|---|
| 4 3<br>1 2 0.25<br>1 4 0.42<br>2 3 1.0<br>| 1<br>0.75<br>0.25<br>0.58<br>|


|  Sample Input 2 |Sample Output 2|
|---|---|
|2 3<br>1 2 0.23<br>2 1 0.99<br>1 2 0.99<br>|0.2377000000<br>0.7623770000|

CPU Time limit 1 second

Memory limit 1024 MB

Difficulty 6.9Hard

Statistics [Show](/problems/assassins/statistics)

![/problems/assassins/file/statement/en/img-0001.jpg](https://open.kattis.com/problems/assassins/file/statement/en/img-0001.jpg)\

---

[Sample data files](/problems/assassins/file/statement/samples.zip)

Authors

[Johan Sannemo](/problem-authors/Johan%20Sannemo) and [Per Austrin](/problem-authors/Per%20Austrin)

Source [KTH Challenge 2019](/problem-sources/KTH%20Challenge%202019)

License [![CreatiDownloadsve Commons License (cc by-sa)](https://licensebuttons.net/l/by-sa/3.0/80x15.png)](https://creativecommons.org/licenses/by-sa/3.0/)
