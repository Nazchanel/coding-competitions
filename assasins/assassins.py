assasins_and_attempts = input()
temp = assasins_and_attempts.split()

assasins = int(temp[0]) # Number of assasins
attempts = int(temp[1]) # Number of lines

lst = []

for i in range(attempts):
    assasin_plan_prob = input()
    temp = assasin_plan_prob.split()
    
    assasin = float(temp[0]) # Current assasin
    plan = float(temp[1]) # Plans to attempt the assasin
    prob = float(temp[2]) # A percentage
    
    lst.extend([[assasin, plan,prob]])
    
    print(lst)
